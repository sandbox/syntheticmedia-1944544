<?php

/*
 * @file
 * Implements hook_form().
 * Weather settings page form
 */
function aprsfiwx_admin_weather_settings_form($node, &$form_state) {
  $form = array();

  $form['overview'] = array(
    '#markup' => t('This interface allows adminstrators to configure settings for the <em>APRS.fi Weather</em> block.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $form['aprsfiwx_apikey_header'] = array(
    '#markup' => t('API Key'),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  $form['aprsfiwx_apikey'] = array(
    '#title' => t('Your aprs.fi API Key'),
    '#description' => t('To use this module you will need an api key from aprs.fi, you will need to register for a free account at <a href="http://aprs.fi">http://aprs.fi</a> and copy your api key from the user settings. (REWRITE).'),
    '#type' => 'textfield', 
    '#default_value' => variable_get('aprsfiwx_apikey', t('')),
    '#required' => TRUE,
  );

  $form['aprsfiwx_weather_stations'] = array(
    '#markup' => t('Weather Stations'),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  $form['aprsfiwx_callsigns'] = array(
    '#title' => t('Call Sign(s)'),
    '#description' => t('Enter at least one WX station callsign.  Separate multiple stations using , (comma) with no spaces.  Do not use trailing commas. Example: D3691,W3NRG-1 Find available WX stations by visiting <a href="http://aprs.fi.">http://aprs.fi</a>.'),
    '#type' => 'textfield', 
    '#default_value' => variable_get('aprsfiwx_callsigns', t('')),
    '#required' => TRUE,
  );

  $form['aprsfiwx_reporting_settings_header'] = array(
    '#markup' => t('Reporting Settings'),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );
  
  $form['aprsfiwx_scale_set'] = array(
    '#title' => t('U.S. units.'),
    '#description' => t('Unticking will toggle metric scale.'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_scale_set', 1),
  );

  $form['aprsfiwx_accuracy'] = array(
    '#title' => t('Accuracy'),
    '#description' => t('Set the desired accuracy (number of decimals to include in readings).  Numbers will round accordingly.  Examples: 0 = 57, 1 = 56.6, 2 = 56.56.'),
    '#type' => 'textfield', 
    '#default_value' => variable_get('aprsfiwx_accuracy', t('0')),
    '#required' => TRUE,
  );
  
  $form['aprsfiwx_custom_date_format'] = array(
    '#title' => t('Date/Time type'),
    '#description' => t('Enter short, medium or long to format time and date per default Drupal date types.  You may also use the machine name of any custom Drupal date type. Extend date functionality with the <a href="http://drupal.org/project/date">date module</a>.'),
    '#type' => 'textfield', 
    '#default_value' => variable_get('aprsfiwx_custom_date_format', t('short')),
    '#required' => TRUE,
  );
  
  $form['fields_h3'] = array(
    '#markup' => t('Weather Reporting Fields'),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  $form['fields_p'] = array(
    '#markup' => '<div class="description">' . t('Select fields to be rendered for each station.') . '</div>',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $form['aprsfiwx_station_name'] = array(
    '#title' => t('Station name'),
    '#description' => t('Station names are passed through t().  Use <a href="http://drupal.org/project/stringoverrides">string overrides module</a> to customize station names.'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_station_name', 1),
  );

  $form['aprsfiwx_station_link'] = array(
    '#title' => t('Include link to WX station on http://aprs.fi?'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_station_link', 1),
  );

  $form['aprsfiwx_temp'] = array(
    '#title' => t('Tempurature'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_temp', 1),
  );

  $form['aprsfiwx_time'] = array(
    '#title' => t('Date/Time'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_time', 1),
  );

  $form['aprsfiwx_pressure'] = array(
    '#title' => t('Pressure'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_pressure', 1),
  );

  $form['aprsfiwx_humidity'] = array(
    '#title' => t('Humidity'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_humidity', 1),
  );

   $form['aprsfiwx_wind_direction'] = array(
    '#title' => t('Wind direction'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_wind_direction', 1),
  );

  $form['aprsfiwx_wind_speed'] = array(
    '#title' => t('Wind speed'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_wind_speed', 1),
  );

  $form['aprsfiwx_wind_gust'] = array(
    '#title' => t('Wind gust'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_wind_gust', 1),
  );
  
  $form['aprsfiwx_rain_1h'] = array(
    '#title' => t('Rainfall over 1hr'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_rain_1h', 1),
  );

  $form['aprsfiwx_rain_24h'] = array(
    '#title' => t('Rainfall over 24hr'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_rain_24h', 1),
  );

  $form['aprsfiwx_rain_mn'] = array(
    '#title' => t('Rainfall since midnight'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_rain_mn', 1),
  );
  
  
  /*
   * Luminosity does not currently work with api.
   * Issue #1945232
   * 
  $form['aprsfiwx_luminosity'] = array(
    '#title' => t('Luminosity'),
    '#type' => 'checkbox', 
    '#default_value' => variable_get('aprsfiwx_luminosity', 1),
  );
  *
  */
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/*
 * Validates APRS.fi weather settings
 */
 
function aprsfiwx_admin_weather_settings_form_validate($form, &$form_state) {

}
 
/*
 * Process validated APRS.fi submission.
 */
 
function aprsfiwx_admin_weather_settings_form_submit($form, &$form_state) {
  // Rebuild the form.
  $form_state['rebuild'] = TRUE;

  // Save APRS.fi variables.
  variable_set('aprsfiwx_apikey', $form_state['values']['aprsfiwx_apikey']);
  variable_set('aprsfiwx_callsigns', $form_state['values']['aprsfiwx_callsigns']);
  variable_set('aprsfiwx_scale_set', $form_state['values']['aprsfiwx_scale_set']);
  variable_set('aprsfiwx_accuracy', $form_state['values']['aprsfiwx_accuracy']);
  variable_set('aprsfiwx_custom_date_format', $form_state['values']['aprsfiwx_custom_date_format']);
  variable_set('aprsfiwx_station_name', $form_state['values']['aprsfiwx_station_name']);
  variable_set('aprsfiwx_station_link', $form_state['values']['aprsfiwx_station_link']);
  variable_set('aprsfiwx_temp', $form_state['values']['aprsfiwx_temp']);
  variable_set('aprsfiwx_time', $form_state['values']['aprsfiwx_time']);
  variable_set('aprsfiwx_pressure', $form_state['values']['aprsfiwx_pressure']);
  variable_set('aprsfiwx_humidity', $form_state['values']['aprsfiwx_humidity']);
  variable_set('aprsfiwx_wind_direction', $form_state['values']['aprsfiwx_wind_direction']);
  variable_set('aprsfiwx_wind_speed', $form_state['values']['aprsfiwx_wind_speed']);
  variable_set('aprsfiwx_wind_gust', $form_state['values']['aprsfiwx_wind_gust']);
  variable_set('aprsfiwx_rain_1h', $form_state['values']['aprsfiwx_rain_1h']);
  variable_set('aprsfiwx_rain_24h', $form_state['values']['aprsfiwx_rain_24h']);
  variable_set('aprsfiwx_rain_mn', $form_state['values']['aprsfiwx_rain_mn']);
  //variable_set('aprsfiwx_luminosity', $form_state['values']['aprsfiwx_luminosity']);  Luminosity does not currently work with api. Issue #1945232.
  // Notify the user.
  drupal_set_message(t('APRS.fi Weather settings saved.'));
}